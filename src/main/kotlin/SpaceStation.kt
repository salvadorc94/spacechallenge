import kotlin.random.Random
import kotlin.random.nextInt

class Item(val name: String, val weight: Int)

interface SpaceShip {
    fun launch() = true
    fun land() = true
    fun canCarry(item : Item) : Boolean
    fun carry(item: Item)
}

open class Rocket(val cost: Int,
                      var weight: Int,
                      val maxWeight : Int) : SpaceShip{

    var hasShield : Boolean = false

    override fun canCarry(item: Item) = weight.plus(item.weight) <= maxWeight
    override fun carry(item: Item) {
        this.weight = weight.plus(item.weight)
    }
}

class U1 : Rocket(cost = 100, weight = 10, maxWeight = 18) {
    override fun launch() : Boolean = Random.nextInt(0..100) > (5 * (weight / maxWeight))
    override fun land() : Boolean = Random.nextInt(0..100) > (weight / maxWeight)
}

class U2: Rocket(cost = 120,weight = 18,maxWeight = 29){
    override fun launch() : Boolean = Random.nextInt(0..100) > (4 * (weight / maxWeight))
    override fun land() : Boolean = Random.nextInt(0..100) > (8 * (weight / maxWeight))
}

fun Rocket.toggleShield() {
    hasShield = !hasShield
}


