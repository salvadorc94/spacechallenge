import java.io.File

enum class RocketType {
    U1, U2,
}

class Simulation {

    fun loadItems(pathName: String) : ArrayList<Item> {
        val items = ArrayList<Item>()
        File(pathName).forEachLine {
            val(name,weight) = it.split("=")
            items.add(Item(name,weight.toInt() / 1000))
        }
        return items
    }

    fun loadU1(items : ArrayList<Item>) : ArrayList<Rocket> {
        return rocketLoader(items, RocketType.U1)
    }

    fun loadU2(items : ArrayList<Item>) : ArrayList<Rocket> {
        return rocketLoader(items, RocketType.U2)
    }

    fun runSimulation(rockets : ArrayList<Rocket>) : Int{

        var budget = 0
        var launchCounter = 0

        rockets.forEach {
            val launched = it.launch()
            val landed = it.land()

            launchCounter += 1
            if(launchCounter == 4){
                it.toggleShield()
                launchCounter = 0
            }

            budget += it.cost
            if(launched && landed) {
                println("Cost: \$${it.cost} Million Dollars, " +
                       "Weight with Cargo: ${it.weight} Tons, " +
                       "Max Weight: ${it.maxWeight} Tons, " +
                       "Success! " +
                       (if (it.hasShield) " This rocket landed with a shield!" else "") )
            }else{
                println("Cost: \$${it.cost} Million Dollars, " +
                        "Weight with Cargo: ${it.weight} Tons, " +
                        "Max Weight: ${it.maxWeight} Tons, " +
                        "**Failure! " +
                        (if (launched) "Rocket Crashed!" else "Rocket Exploded!")+
                        (if (it.hasShield) " This rocket had a shield!" else ""))

                if(it.hasShield){
                    it.toggleShield()
                }

                val result = relaunchRocket(it,launchCounter)
                budget += result.first
                launchCounter = result.second
            }
        }
        return budget
    }
}

fun relaunchRocket(rocket: Rocket , counter: Int) : Pair<Int, Int> {
    var relaunchCounter = 1
    var failBudget = 0

    var launchCounter = counter
    launchCounter += 1
    if(launchCounter == 4){
        rocket.toggleShield()
        launchCounter = 0
    }

    println("*".repeat(relaunchCounter)+" Relaunching Failed Rocket"+
            (if(rocket.hasShield) " This Rocket landed with a shield!" else ""))
    failBudget+= rocket.cost


    var launched = rocket.launch()
    var landed = rocket.land()

    //Just in case, it would be really rare to enter here, but it still has some probability
    while (!launched && !landed){

        rocket.toggleShield()
        launchCounter += 1
        if(launchCounter == 4){
            rocket.toggleShield()
            launchCounter = 0
        }

        failBudget += rocket.cost
        relaunchCounter += 1
        launched = rocket.launch()
        landed = rocket.land()
        println("*".repeat(relaunchCounter)+ " Relaunching"+
                (if(rocket.hasShield) " This Rocket landed with a shield!" else ""))
    }
    println("\tRelaunched: $relaunchCounter times. Cost: \$$failBudget Million Dollars.")

    return Pair(failBudget, launchCounter)
}

fun rocketLoader(items : ArrayList<Item>, rocketType: RocketType) : ArrayList<Rocket> {
    val rockets = ArrayList<Rocket>()

    var rocket = when(rocketType) {
        RocketType.U1 -> U1()
        RocketType.U2 -> U2()
    }
    //var rocket = U1()

    items.sortBy {  it.weight }
    while (items.isNotEmpty()){
        for(i in items.indices.reversed()){
            if (rocket.canCarry(items[i])){
                rocket.carry(items[i])
                items.remove(items[i])
            }
        }
        rockets.add(rocket)
        if(items.isNotEmpty()){
            rocket = when(rocketType) {
                RocketType.U1 -> U1()
                RocketType.U2 -> U2()
            }
        }
    }
    return rockets
}

/*fun rocketLoader(items : ArrayList<Item>) : ArrayList<U1> {
    val rockets = ArrayList<U1>()

    var rocket = U1()
    for(i in items.indices){
        if (rocket.canCarry(items[i])){
            rocket.carry(items[i])
            //If it is the last element of the list
            if(i >= items.size-1){
                rockets.add(rocket)
                return rockets
            }
        }else{
            rockets.add(rocket)
            rocket = U1()
            rocket.carry(items[i])
        }
    }
    return rockets
}*/